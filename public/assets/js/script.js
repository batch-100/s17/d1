// alert("message")
// 
/*
JSON Data format na ginagamit natin to store and transfer data

	1. supports almost all data types
 */
//syntax
let person = {
    "name": "Juan",
    "weight": 175,
    "age": 40,
    "car": ["Honda", "Toyota"],
    "motorbike": {
        "brand": "Suzuki",
        "color": "red"
    }
}

//there are two methods
//Methods: stringify() and parse() - cinoconvert niya yung json to javascript
//Stringify()
//Supposed my js tayo

let cat = {
    name: "mashiro",
    color: "brown",
    breed: "siameses"
}
//kapag inistringify na to, ganto magiging itsura
//yung stringify simply pass in a js object you want converted to JSON
let catJSON = JSON.stringify(cat)
console.log(catJSON);

// parse()
//dapat po string ung ilalagay ngoDBatin sa JSON.parse()
let jsonCat = '{"name": "mashiro", "color": "brown", "breed": "siameses"}';
let catTwo = JSON.parse(jsonCat);
console.log(catTwo);